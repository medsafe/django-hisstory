Django-hisstory
--------------

Django-hisstory, is a lightweight read-only django model history app.

Installation
------------

As usual:
    pip install django-hisstory

Add  `hisstory` to INSTALLED_APPS.
